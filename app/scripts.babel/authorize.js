'use strict';

window.addEventListener('message', receiveMessageWrapper(function (host, callback) {
  document.getElementById('host').innerText = host;

  document.getElementById('cancel').onclick = function () {
    callback(false);
    window.close();
  };

  document.getElementById('authorize').onclick = function () {
    const isAutomatic = document.getElementById('automatic').checked;
    callback({automatic: isAutomatic});

    window.close();
  };
}), false);